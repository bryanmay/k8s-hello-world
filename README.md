# K8S hello world
[![pipeline status](https://gitlab.com/bryanmay/k8s-hello-world/badges/master/pipeline.svg)](https://gitlab.com/bryanmay/k8s-hello-world/commits/master)
[![coverage report](https://gitlab.com/bryanmay/k8s-hello-world/badges/master/coverage.svg)](https://gitlab.com/bryanmay/k8s-hello-world/commits/master)

## Overview
The goal of this project is to learn about how to use gitlab to deploy apps into cloud hosted Kubernetes solution. For this, I chose Google Kubernetes Engine as my deployment target. I wanted to see how the integration between gitlab and GKE/EKS works. I also wanted to learn about how the auto devops feature works to build out-of-the box testing and deployment capabilities.

## Inspiration
 - I used some of the GKE docs [here](https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app) to setup my Google Cloud Platform account since it was a pre-requisite to use the GKE integration in gitlab.
 - Next I followed some of the steps specific to a containerized python/flask app in [this example ](https://about.gitlab.com/blog/2019/11/18/gitops-prt-3/). *Note: I used GKE not EKS*

# Steps taken
1. fork the project from https://gitlab.com/gitops-demo/apps/my-python-app4 into your namespace
1. Create GCP account that you have admin control on.
1. go to Operations >> Kubernetes on a gitlab project.
1. login with gcp credentials
1. create a gitlab-controlled k8s cluster on gke
1. Go to GCP console to see the k8s cluster being created
1. Install helm, prometheus, and k8s ingress services on the cluster. (Operations >> Kubernetes >> `your new cluster name`)
1. Copy the Ingress endpoint ip to a CI/CD Variable called `KUBE_INGRESS_BASE_DOMAIN` (Settings >> CI/CD Settings >> Variables)
1. While you're here, create a variable called `CI_PROJECT_PATH_SLUG` with your repo slug. (These are used in the production deployment stage of your CI Pipeline)
1. Make update to `main.py`

## Deployment
This will deploy to `http://CI_PROJECT_PATH_SLUG.KUBE_INGRESS_BASE_DOMAIN.com` or https://k8s-hello-world.35.229.32.54.nip.io/
